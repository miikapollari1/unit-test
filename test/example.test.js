const expect = require("chai").expect;
const { before, after, it } = require("mocha");
const mylib = require("../src/mylib");

describe("Testing mylib functions", () => {
  before(() => {
    console.log("\nTesting mylib functions - begin\n");
  });
  describe("Sum", () => {
    it("Sum of two numbers should be 15", () => {
      const result = mylib.sum(10, 5);
      expect(result).to.equal(15);
    });
  });

  describe("Random", () => {
    it("Should generate random number", () => {
      const result = mylib.random();
      expect(result).not.equal(null);
    });
  });

  describe("Array generator", () => {
    it("should generate array lenght of 3", () => {
      const array = mylib.arrayGen();
      expect(array).lengthOf(3);
    });

    it("Array numbers should be 1, 2, 3", () => {
      const array = mylib.arrayGen();
      expect(array[0]).to.equal(1);
      expect(array[1]).to.equal(2);
      expect(array[2]).to.equal(3);
    });
  });

  describe("Divide", () => {
    it("Number is not divided by zero", () => {
      const divide_a = 2;
      const divide_b = 5;
      const result = mylib.divide(divide_a, divide_b);
      expect(divide_a).not.equal(0);
    });
  });

  after(() => {
    console.log("\nTesting mylib functions - completed");
  });
});
